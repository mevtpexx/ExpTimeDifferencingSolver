#include <cmath>
#include "ksequationproperties.h"

KSEquationProperties::KSEquationProperties (double length)
{
    basic_wavevector=2*M_PI/length;
}

double KSEquationProperties::lambda(int mode_index)const
{
    return pow(mode_index,2)*pow(basic_wavevector,2)-pow(mode_index,4)*pow(basic_wavevector,4);
}

double KSEquationProperties::nonlinear(bool imag,ComplexVector &coefficients, int mode_index)const
{
    if(linearized)
    {
        return 0;
    }
    else
    {
        double sum=0;
        if(imag)
        {
            for(int n=1-coefficients.getSize();n<coefficients.getSize();n++)
            {
                if(std::abs(mode_index-n)<coefficients.getSize())
                {   
                    sum=sum+n*basic_wavevector*(coefficients.getCoefficient(1,n)*coefficients.getCoefficient(1,mode_index-n)-coefficients.getCoefficient(0,mode_index-n)*coefficients.getCoefficient(0,n));
                }
            }
        }
        else
        {
            for(int n=1-coefficients.getSize();n<coefficients.getSize();n++)
            {
                if(std::abs(mode_index-n)<coefficients.getSize())
                {   
                    sum=sum+n*basic_wavevector*(coefficients.getCoefficient(0,n)*coefficients.getCoefficient(1,mode_index-n)+coefficients.getCoefficient(0,mode_index-n)*coefficients.getCoefficient(1,n));
                }
            }
        }
        return sum;
    }
}
