#include <iostream>
#include <fstream>
#include "complexvector.h"
#include "dust.h"

#ifndef OUTPUT_H
#define OUTPUT_H

class Output
{
private:
    std::ofstream outfile;
    std::vector<std::pair<double,ComplexVector>> solution;
    std::string position_array;
public:
    double getLocaleSpaceValue(const ComplexVector&,double position) const;
    void save(double,ComplexVector);
    void save(double,Dust&);
    void hardsave(std::string,ComplexVector);
    void fileprint(std::string);
    void fileprint(std::string,int);
    void fileprintPos(std::string);
    void clear();
};

#endif // EQUATIONPROPERTIES_H
