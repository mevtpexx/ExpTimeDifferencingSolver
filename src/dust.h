#include "particle.h"

#ifndef DUST_H
#define DUST_H

class Dust
{
private:
    std::vector<Particle> particles;
public:
    Dust(int,double,double);
    void equallyDistribute(int,double,double);
    void move(ComplexVector,double);
    int getNumber()const;
    Particle getParticle(int)const;
};

#endif // DUST_H
