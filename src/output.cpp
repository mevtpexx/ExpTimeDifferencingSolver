#include <cmath>
#include "output.h"

double Output::getLocaleSpaceValue(const ComplexVector& modes,double position) const
{
    double sum=0;
    for(int mode=0;mode<modes.getSize();mode++)
    {   
        sum+=modes.getCoefficient(0,mode)*(cos(mode*position)+cos(-mode*position))-modes.getCoefficient(1,mode)*(sin(mode*position)-sin(-mode*position));
    }
    return sum;
}

void Output::save(double time,ComplexVector mode_states)
{
    solution.push_back(std::make_pair(time,mode_states));
}

void Output::save(double time,Dust& dust)
{
    for(int i=0;i<dust.getNumber();i++)
    {
        position_array.append(std::to_string(time)+"\t"+std::to_string(dust.getParticle(i).getPosition())+"\n");
    }
}

void Output::hardsave(std::string file,ComplexVector state)
{
    outfile.open(file,std::ios::trunc);
    for(int mode_index=0;mode_index<state.getSize();mode_index++)
    {
        outfile << state.getCoefficient(0,mode_index) << "\t" << state.getCoefficient(1,mode_index) << "\t";
    }
    outfile.close();
}

void Output::fileprint(std::string file)
{
    outfile.open(file,std::ios::trunc);
    for(std::vector<std::pair<double,ComplexVector>>::const_iterator it=solution.begin();it!=solution.end();it++)
    {
        outfile << it->first;
        for(int mode_index=0;mode_index<it->second.getSize();mode_index++)
        {
            outfile << "\t" << it->second.getCoefficient(0,mode_index);
        }
        outfile << std::endl;
    }
    outfile.close();
}

void Output::fileprint(std::string file,int resolution)
{
    outfile.open(file,std::ios::trunc);
    double step=2*M_PI/resolution;
    for(std::vector<std::pair<double,ComplexVector>>::const_iterator it=solution.begin();it!=solution.end();it++)
    {
        outfile << it->first;
        for(int i=0;i<resolution;i++)
        {
            outfile << "\t" << getLocaleSpaceValue(it->second,i*step);
        }
        outfile << std::endl;
    }
    outfile.close();
}

void Output::fileprintPos(std::string file)
{
    outfile.open(file,std::ios::trunc);
    outfile << position_array;
    outfile.close();
}

void Output::clear()
{
    solution.clear();
    outfile.close();
}
