#include <vector>
#include <fstream>

#ifndef COMPLEXVECTOR_H
#define COMPLEXVECTOR_H

class ComplexVector
{
private:
    std::vector<double> coefficients_real;
    std::vector<double> coefficients_imag;
public:
    void initialize(int,double,double);
    int getSize()const;
    void setCoefficient(bool,int,double);
    double getCoefficient(bool,int) const;
    ComplexVector();
    ComplexVector(std::string);
};

#endif // COMPLEXVECTOR

