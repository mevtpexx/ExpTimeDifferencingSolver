#include "complexvector.h"
#include "equationproperties.h"
#ifndef EXPTIMEDIFFERENCING_H
#define EXPTIMEDIFFERENCING_H

class ExpTimeDifferencing
{
private:
    ComplexVector coefficients;
    ComplexVector temp_coefficients;
    double timestep;
    EquationProperties *equation;
public:
    void setCoefficients(ComplexVector);
    void iterate(double tstep);
    void doNothing(double tstep);
    ComplexVector getCurrentState()const;
    ExpTimeDifferencing(EquationProperties*,int);
};

#endif // EXPTIMEDIFFERENCING_H
