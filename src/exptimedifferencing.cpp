#include <iostream>
#include <vector>
#include <cmath>

#include "exptimedifferencing.h"

ExpTimeDifferencing::ExpTimeDifferencing(EquationProperties *eq,int size)
{
    equation=eq;
    coefficients.initialize(size,1,0);
    temp_coefficients.initialize(size,1,0);
}

void ExpTimeDifferencing::setCoefficients(ComplexVector new_coeff)
{
    coefficients=new_coeff;
}

void ExpTimeDifferencing::iterate(double tstep)
{
    for(int n=0;n<coefficients.getSize();n++)
    {
        if(!(equation->lambda(n)==0))
        {
            //real part
            temp_coefficients.setCoefficient(0,n,coefficients.getCoefficient(0,n)*exp(equation->lambda(n)*tstep)+equation->nonlinear(0,coefficients,n)*(exp(equation->lambda(n)*tstep)-1)/(equation->lambda(n)));
            //imaginary part
            temp_coefficients.setCoefficient(1,n,coefficients.getCoefficient(1,n)*exp(equation->lambda(n)*tstep)+equation->nonlinear(1,coefficients,n)*(exp(equation->lambda(n)*tstep)-1)/(equation->lambda(n)));
        }
    }
    for(int n=0;n<coefficients.getSize();n++)
    {
        if(!(equation->lambda(n)==0))
        {
            //real part
            coefficients.setCoefficient(0,n,temp_coefficients.getCoefficient(0,n)+(equation->nonlinear(0,temp_coefficients,n)-equation->nonlinear(0,coefficients,n))*(exp(equation->lambda(n)*tstep)-1-equation->lambda(n)*tstep)/(pow(equation->lambda(n),2)*tstep));
            //imaginary part
            coefficients.setCoefficient(1,n,temp_coefficients.getCoefficient(1,n)+(equation->nonlinear(1,temp_coefficients,n)-equation->nonlinear(1,coefficients,n))*(exp(equation->lambda(n)*tstep)-1-equation->lambda(n)*tstep)/(pow(equation->lambda(n),2)*tstep));
        }
    }
}

void ExpTimeDifferencing::doNothing(double tstep)
{
}

ComplexVector ExpTimeDifferencing::getCurrentState()const
{
    return coefficients;
}
