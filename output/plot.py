import math
import cmath
import numpy as np
from matplotlib import pyplot as plt

data=np.loadtxt("ufield.dat")

time=np.transpose(data)[0]

u=np.transpose(data)[1:,:]

x=np.linspace(0, 2*math.pi, num=314)

plt.contourf(time,x,u,100,cmap=plt.cm.Spectral)
plt.show()

data=np.loadtxt("mode_states.dat")

plt.plot(time,data[:,1:])
plt.show()

data=np.loadtxt("pos_list.dat");

print data.shape
plt.plot(data[:5000,0],data[:5000,1],'b.')
plt.show()
